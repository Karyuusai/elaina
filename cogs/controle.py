from discord.ext import commands
import discord

class Controle(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def controleReacoes(self, bot, mensagem, tamTotal, tamFields, primeiraPagina, ultimaPagina):

        listaReacoes        = ['1️⃣','2️⃣','3️⃣','4️⃣','5️⃣','▶️']
        listaReacoesInversa = ['▶️','5️⃣','4️⃣','3️⃣','2️⃣','1️⃣']

        if primeiraPagina:
            if tamTotal > 0:
                tamFields = tamFields + 1

            for numeroInteracoes in range(0, tamFields):
                await mensagem.add_reaction(listaReacoes[numeroInteracoes])

            await mensagem.add_reaction('❌')

        elif not primeiraPagina and ultimaPagina:           
            for numeroInteracoes in range(0,(6-tamFields)):
                await mensagem.remove_reaction(listaReacoesInversa[numeroInteracoes],bot.user)
        else:
            pass


def setup(bot):
    bot.add_cog(Controle(bot))