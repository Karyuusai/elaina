
import asyncio
import discord
from discord.ext import commands
from Util.getPerms import getPerms, getOverwrites 
from Util.buildSimpleEmbed import simpleEmbed
from dataBase.dataBase import DataBase

db = DataBase()

def myCheck(ctx):
    return ctx.author.id == 403360588249563137 \
          or ctx.author == ctx.guild.owner \
              or ctx.author.id == 339596484007690260 \
                  or ctx.author.id == 510170006470918154

class Template(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases=["sv"])
    @commands.check(myCheck)
    async def save(self, ctx):
        try:
            preview = getCategorys(ctx)
            await ctx.send(embed=simpleEmbed("Novo backup salvo!", f"{preview}", ctx.author, "SaveBackup"))
        except:
            raise commands.errors.DiscordException

    @commands.command(aliases=["bp"])
    @commands.check(myCheck)
    async def backup(self, ctx):
        if ctx.author.guild.id == 410856116994441217:
            return await ctx.send("Guilda errada!")
        
        try:
            msg = await ctx.send("Gostaria de carregar a template para o servidor?(y/yes)(n/no)")
            response = await self.bot.wait_for("message", timeout = 20.0, check = lambda m: m.author == ctx.author)
            response = response.content

            if response == "y" or response == "yes":
                await getDbRoles(ctx)
                await getDbCategories(ctx)
            
            elif response == "n" or response == "no":
                return await msg.delete()

            else:
                warning = await ctx.send("Resposta inválida!")
                await warning.delete(delay=5)
                return await msg.delete()

            await ctx.send(embed = simpleEmbed("Guilda restaurada!", "O servidor foi reconstruido com base no último backup.", ctx.author, "Backup"))
        except asyncio.TimeoutError:
            await msg.delete()
            return

def getCategorys(ctx):

    GUILD_SAVE = {}
    GUILD_SAVE["_id"] = ctx.author.guild.id
    GUILD_SAVE["roles"] = getRoles(ctx)

    preview = "⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼⎼\n"

    for category in ctx.message.guild.categories:
        GUILD_SAVE[category.name] = {}
        GUILD_SAVE[category.name]["permissions"] = getCategoryPerms(category)
        GUILD_SAVE[category.name]["channels"] = {}

        preview += f"\n{category}  +\n"

        for channel in category.channels:
            GUILD_SAVE[category.name]["channels"][channel.name] = {}
            GUILD_SAVE[category.name]["channels"][channel.name]["type"] = str(channel.type)
            GUILD_SAVE[category.name]["channels"][channel.name]["permissions"] = getChannelPerm(channel)

            preview += f"  #{channel}\n"

    db.save(ctx, GUILD_SAVE)
    return preview

def getRoles(ctx):
    tmp = {}
    for role in ctx.author.guild.roles:

        if role.is_default():
            continue

        if role.is_bot_managed():
            continue

        tmp[role.name] = {}
        tmp[role.name]["permissions"] = dict(role.permissions)
        tmp[role.name]["color"] = str(role.color)
        tmp[role.name]["hoist"] = role.hoist
        tmp[role.name]["mentionable"] = role.mentionable
      
    return tmp

def getChannelPerm(channel):
    tmp = {}
    perms = dict(channel.overwrites)
    
    for perm in perms.items() :
        tmp[str(perm[0])] = dict(perm[1])

    return tmp

async def getDbRoles(ctx):
    try:
        itens = db.find(ctx)
        roles = itens["roles"]
        guild = ctx.guild
        ROLE_POSITIONS = {}
        i = 1
        try:
            for role, permissions in roles.items():
                perms = getPerms(permissions['permissions'])
                hoist = permissions['hoist']
                color = permissions['color']
                mentionable = permissions['mentionable']

                color = getColor(color)
                role = await buildRoles(guild, role, perms, color, hoist, mentionable)
                ROLE_POSITIONS[role] = i
                i += 1
        except:
            pass
        await editRolesPositions(ROLE_POSITIONS, guild)
    except:
        raise commands.errors.DiscordException

    
def getColor(color):
    color = discord.Colour(int(color[1:], 16))
    return color

async def buildRoles(guild, role, perms, color, hoist, mentionable):
    role = await guild.create_role(name=str(role), permissions=perms, color=color, hoist=hoist, mentionable=mentionable)
    return role

async def editRolesPositions(positions, guild):
    await guild.edit_role_positions(positions=positions)

async def getDbCategories(ctx):
    itens = db.find(ctx)

    del itens["_id"]
    del itens["roles"]

    guild = ctx.guild
    try:
        for category, permissions in itens.items():
            perms = buildOverwrites(permissions, guild=ctx.guild)
            categoryCreated = await buildCategories(guild, category, perms)
            
            for channel, perm in itens[category]["channels"].items():
                overwrites = buildOverwrites(perm, guild)
                await buildChannels(guild, str(perm["type"]), categoryCreated, str(channel), overwrites)
    except:
        pass    

async def buildCategories(guild, category, overwrites):
    category = await guild.create_category(name=str(category), overwrites=overwrites)
    return category

async def buildChannels(guild, channelType, category, channel, overwrites):
    if channelType == "text":
        channel = await guild.create_text_channel(name=str(channel), category=category, overwrites=overwrites)
        return

    channel = await guild.create_voice_channel(name=str(channel), category=category, overwrites=overwrites)
    return  

def getCategoryPerms(category):
    categoryPerms = {}
    overwritesPerms = dict(category.overwrites)
    for role, perm in overwritesPerms.items():
        categoryPerms[str(role)] = dict(perm)
    
    return categoryPerms

def buildOverwrites(rolesAndPerms, guild):
    overwrite = {}
    for role, perm in rolesAndPerms["permissions"].items():
        role = discord.utils.get(guild.roles, name=str(role))
        perm = getOverwrites(perm)
        overwrite[role] = perm
    
    return overwrite


def setup(bot):
    bot.add_cog(Template(bot))




    