import discord
from discord.ext import commands
from dataBase.dataBase import DataBase

db = DataBase()

class On_guild_join(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_guild_join(self, guild):
        prefix = {"_id": guild.id,
                  "Prefix"  : '.'}
        print(guild.id)
        prefix_collection = db.db['prefix']
        itens_prefix = prefix_collection.find()
        
        for item in itens_prefix:
            if item['_id'] == guild.id:
                return

        prefix_collection.insert_one(prefix)

def setup(bot):
    bot.add_cog(On_guild_join(bot))
