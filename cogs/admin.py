import discord
from discord.ext import commands
from dataBase.dataBase import DataBase

db = DataBase()

class Admin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.is_owner()
    @commands.command()
    async def test(self, ctx, msgID:int):
        msg = await ctx.fetch_message(msgID)
        await msg.add_reaction('😀')
    
    @commands.is_owner()
    @commands.command()
    async def send(self, ctx, fileName):
        path = (__file__.replace('cogs\\admin.py',f'Resources\images\{fileName}'))
        f = discord.File(path)
        await ctx.send(file=f)

    @commands.has_permissions(administrator=True)
    @commands.command(aliases = ["cp"])
    async def change_prefix(self, ctx, prefix):
        prefixDB = db.prefix
        prefixDB.find_one_and_update(
            {"_id": ctx.author.guild.id},
                {"$set":
                    {"Prefix":f"{prefix}"}
    },upsert=True
)
        await ctx.send(f"Prefixo de `{ctx.author.guild.name}` agora é `{prefix}`")

    @commands.has_permissions(administrator=True)
    @commands.command()
    async def purge(self, ctx, amount:int):
        await ctx.message.delete()
        await ctx.channel.purge(limit = amount)
        warning_delete = await ctx.send(f'{ctx.author.mention} *deleteou {amount} mensagens*')
        await warning_delete.delete(delay = 4)   

def setup(bot):
    bot.add_cog(Admin(bot))
