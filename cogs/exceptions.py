from discord.ext import  commands
from Util.buildSimpleEmbed import simpleEmbed
from dataBase.dataBase import DataBase

db = DataBase()

class commandsExceptions(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):

        if isinstance (error, commands.MissingPermissions) or isinstance(error, commands.CheckFailure):
            embed = simpleEmbed("Acesso não autorizado", 
            "❌ Você não tem permissão para utilizar esse comando.", ctx.author, "MissingPermissions")
            await ctx.send(embed = embed)

        elif isinstance(error, commands.CommandNotFound):
            prefix = db.get_prefix(ctx.message)
            embed = simpleEmbed("Comando não encontrado", f"Digite `{prefix}`help e veja os comandos disponíveis.", ctx.author, "CommandNotFound")
            await ctx.send(embed = embed)
        
        elif isinstance(error, commands.NSFWChannelRequired):
            embed = simpleEmbed("Canal errado!!!", "Este comando só pode ser executado em um canal +18.", ctx.author, "NSFW")
            await ctx.send(embed=embed)

        else:
            embed = simpleEmbed("Algo saiu errado!", "Talvez não seja nada, tente novamente mais tarde.", ctx.author, "SomethingWrong")
            await ctx.send(embed = embed)   
   
def setup(bot):
    bot.add_cog(commandsExceptions(bot))