from discord.ext import commands
import discord
import Util.getAnime    as anime
import Util.animeEmbed  as animeEmbedFunc
import Util.gerarCores  as cor
import math
import asyncio

class Anime(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(aliases = ["anime"])
    async def animeCommand(self, ctx, title):
        animeOBJ   = anime.animeList(title)

        if len(animeOBJ['data']['Page']['media']) == 0:
            await ctx.send("Sem resultados")
            return

        animeEmbed = discord.Embed(
             title = "Pesquisa:",
             color = cor.gerarCorRandom()
        )
        animeEmbed.set_thumbnail(url=ctx.author.avatar_url)
        dicionarioReacoes= {1:'1️⃣',2:'2️⃣',3:'3️⃣',4:'4️⃣',5:'5️⃣',6:'▶️'}
        listaId          = []
        final            = 5
        primeiraPagina   = True
        ultimaPagina     = False
        controle         = self.bot.get_cog('Controle')
        paginas          = math.ceil(len(animeOBJ['data']['Page']['media'])/final)
        paginaAtual      = 1
        while True:

            if len(animeOBJ['data']['Page']['media']) <= final:
                final        = len(animeOBJ['data']['Page']['media'])
                ultimaPagina = True

            animeEmbed.set_footer(text=f'Página {paginaAtual}/{paginas}',icon_url=self.bot.user.avatar_url)

            for i in range (0, final):
                animeName = animeOBJ['data']['Page']['media'][0]['title']['userPreferred']
                animeId   = animeOBJ['data']['Page']['media'][0]['id']                      
                animeEmbed.add_field(name=f'- anime#{i+1}', value=animeName, inline=False)
                listaId.append(animeId)                                                     
                del animeOBJ['data']['Page']['media'][0]   

            if primeiraPagina:
                embedName = await ctx.send(embed = animeEmbed)
            else:
                await embedName.edit(embed = animeEmbed)

            await controle.controleReacoes(self.bot, embedName,len(animeOBJ['data']['Page']['media']),len(animeEmbed._fields),primeiraPagina,ultimaPagina)

            try:
                reaction, user = await self.bot.wait_for('reaction_add', timeout=40.0, check = lambda reaction, user: user == ctx.author)
            except asyncio.TimeoutError:
                await embedName.delete()
                await ctx.send("*Tempo Esgotado.*")
                return
            else:
                pass

            if reaction.emoji in dicionarioReacoes.values() or reaction.emoji == '❌':
                for reacaoEscolhida in dicionarioReacoes:

                    if reaction.emoji == '❌':
                        await embedName.delete()
                        return

                    elif reaction.emoji == dicionarioReacoes[reacaoEscolhida] and reaction.emoji != dicionarioReacoes[6]:
                        await embedName.delete()
                        animeEmbed = animeEmbedFunc.animeEmbed(listaId[reacaoEscolhida-1])
                        animeEmbed.set_author(name=ctx.author.name,icon_url=ctx.author.avatar_url)
                        await ctx.send(embed=animeEmbed)
                        return

                    elif reaction.emoji == dicionarioReacoes[6]:
                        animeEmbed.clear_fields()
                        primeiraPagina  = False
                        listaId         = []
                        paginaAtual     = paginaAtual + 1
                        await embedName.remove_reaction(reaction.emoji,user)
                        break

                    else:
                        pass
            else:
                await ctx.send("*Não há nenhuma função atribuída a esse emoji.*")
                await embedName.delete()
                return


def setup(bot):
    bot.add_cog(Anime(bot))