import json
import requests

def animeList(title):
    query= '''
    query ($query: String) {
        Page {
            media(search: $query,type:ANIME,isAdult:false,sort:SEARCH_MATCH) {
                id
                title {
                    userPreferred
                }
            }
        }
    }'''
    variables = {
        'query' : title,
    }
    url = 'https://graphql.anilist.co'
    response = requests.post(url, json={'query': query, 'variables': variables})
    json_obj = json.loads(response.content)
    return json_obj

# def animeInfo(title):
#     query = """
#     query ($title: String) {
#         Media (search: $title, type:ANIME) {
#             title {
#                 userPreferred
#             }
#             description
#             status
#             siteUrl
#             episodes
#             idMal
#             coverImage{
#                 extraLarge
#             }
#             bannerImage
#             format
#             genres
#             meanScore
#             duration
#             id
#         }
#     }
#     """
#     variables = {
#             'title': title
#     }
#     url = 'https://graphql.anilist.co'
#     response    = requests.post(url, json={'query': query, 'variables': variables})
#     json_obj    = json.loads(response.content)
#     animeTitle  = json_obj['data']['Media']['title']['userPreferred']
#     idMal       = str(json_obj['data']['Media']['idMal'])
#     animeUrl    = 'https://myanimelist.net/anime/'+ idMal
#     status      = json_obj['data']['Media']['status']
#     episodios   = json_obj['data']['Media']['episodes']
#     image       = json_obj['data']['Media']['coverImage']['extraLarge']
#     bannerImage = json_obj['data']['Media']['bannerImage']
#     tipo        = json_obj['data']['Media']['format']
#     generos     = json_obj['data']['Media']['genres']
#     score       = json_obj['data']['Media']['meanScore']
#     duracao     = json_obj['data']['Media']['duration']
#     descricao   = json_obj['data']['Media']['description']

#     return  animeTitle, animeUrl, status, episodios, image, bannerImage, tipo, generos, score, duracao, descricao

def animeInfo(idAnime):
    query = """
    query ($id: Int) {
        Media (id: $id, type:ANIME) {
            title {
                userPreferred
            }
            description
            status
            siteUrl
            episodes
            idMal
            coverImage{
                extraLarge
            }
            bannerImage
            format
            genres
            meanScore
            duration
            id
        }
    }
    """
    variables = {
            'id': idAnime
    }
    url = 'https://graphql.anilist.co'
    response    = requests.post(url, json={'query': query, 'variables': variables})
    json_obj    = json.loads(response.content)
    animeTitle  = json_obj['data']['Media']['title']['userPreferred']
    idMal       = str(json_obj['data']['Media']['idMal'])
    animeUrl    = 'https://myanimelist.net/anime/'+ idMal
    status      = json_obj['data']['Media']['status']
    episodios   = json_obj['data']['Media']['episodes']
    image       = json_obj['data']['Media']['coverImage']['extraLarge']
    bannerImage = json_obj['data']['Media']['bannerImage']
    tipo        = json_obj['data']['Media']['format']
    generos     = json_obj['data']['Media']['genres']
    score       = json_obj['data']['Media']['meanScore']
    duracao     = json_obj['data']['Media']['duration']
    descricao   = json_obj['data']['Media']['description']

    return  animeTitle, animeUrl, status, episodios, image, bannerImage, tipo, generos, score, duracao, descricao
