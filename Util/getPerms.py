import discord

def getPerms(rolesPerms):
    perms = discord.Permissions(
        create_instant_invite = rolesPerms['create_instant_invite'],
        kick_members = rolesPerms['kick_members'],
        ban_members = rolesPerms['ban_members'],
        administrator = rolesPerms['administrator'],
        manage_channels = rolesPerms['manage_channels'],
        manage_guild = rolesPerms['manage_guild'],
        add_reactions = rolesPerms['add_reactions'],
        view_audit_log = rolesPerms['view_audit_log'], 
        priority_speaker = rolesPerms['priority_speaker'], 
        stream = rolesPerms['stream'],
        read_messages = rolesPerms['read_messages'],
        send_messages = rolesPerms['send_messages'],
        send_tts_messages = rolesPerms['send_tts_messages'],
        manage_messages = rolesPerms['manage_messages'], 
        embed_links = rolesPerms['embed_links'],
        attach_files = rolesPerms['attach_files'],
        read_message_history = rolesPerms['read_message_history'],
        mention_everyone = rolesPerms['mention_everyone'], 
        external_emojis = rolesPerms['external_emojis'], 
        view_guild_insights = rolesPerms['view_guild_insights'],
        connect = rolesPerms['connect'],
        speak = rolesPerms['speak'],
        mute_members = rolesPerms['mute_members'],
        deafen_members= rolesPerms['deafen_members'],
        move_members = rolesPerms['move_members'],
        use_voice_activation = rolesPerms['use_voice_activation'],
        change_nickname = rolesPerms['change_nickname'],
        manage_nicknames = rolesPerms['manage_nicknames'],
        manage_roles = rolesPerms['manage_roles'],
        manage_webhooks = rolesPerms['manage_webhooks'],
        manage_emojis = rolesPerms['manage_emojis'],
        use_slash_commands = rolesPerms['use_slash_commands'], 
        request_to_speak = rolesPerms['request_to_speak'])
    return perms

def getOverwrites(rolesPerms):
    perms = discord.PermissionOverwrite(
        create_instant_invite = rolesPerms['create_instant_invite'],
        kick_members = rolesPerms['kick_members'],
        ban_members = rolesPerms['ban_members'],
        administrator = rolesPerms['administrator'],
        manage_channels = rolesPerms['manage_channels'],
        manage_guild = rolesPerms['manage_guild'],
        add_reactions = rolesPerms['add_reactions'],
        view_audit_log = rolesPerms['view_audit_log'], 
        priority_speaker = rolesPerms['priority_speaker'], 
        stream = rolesPerms['stream'],
        read_messages = rolesPerms['read_messages'],
        send_messages = rolesPerms['send_messages'],
        send_tts_messages = rolesPerms['send_tts_messages'],
        manage_messages = rolesPerms['manage_messages'], 
        embed_links = rolesPerms['embed_links'],
        attach_files = rolesPerms['attach_files'],
        read_message_history = rolesPerms['read_message_history'],
        mention_everyone = rolesPerms['mention_everyone'], 
        external_emojis = rolesPerms['external_emojis'], 
        view_guild_insights = rolesPerms['view_guild_insights'],
        connect = rolesPerms['connect'],
        speak = rolesPerms['speak'],
        mute_members = rolesPerms['mute_members'],
        deafen_members= rolesPerms['deafen_members'],
        move_members = rolesPerms['move_members'],
        use_voice_activation = rolesPerms['use_voice_activation'],
        change_nickname = rolesPerms['change_nickname'],
        manage_nicknames = rolesPerms['manage_nicknames'],
        manage_roles = rolesPerms['manage_roles'],
        manage_webhooks = rolesPerms['manage_webhooks'],
        manage_emojis = rolesPerms['manage_emojis'],
        use_slash_commands = rolesPerms['use_slash_commands'], 
        request_to_speak = rolesPerms['request_to_speak'])
    return perms