import random

def gerarCorRandom():
    listaCores = [0x1abc9c, #teal
                  0x11806a, #dark_teal
                  0x2ecc71, #green
                  0x1f8b4c, #dark_green
                  0x3498db, #blue
                  0x206694, #dark_blue
                  0x9b59b6, #purple
                  0x71368a, #dark_purple
                  0xe91e63, #magenta
                  0xad1457, #dark_magenta
                  0xf1c40f, #gold
                  0xc27c0e, #dark_gold
                  0xe67e22, #orange
                  0xa84300, #dark_orange
                  0xe74c3c, #red
                  0x992d22, #dark_red
                  0x95a5a6, #lighter_grey
                  0x607d8b, #dark_grey
                  0x979c9f, #light_grey
                  0x546e7a, #darker_grey
                  0x7289da, #blurple
                  0x99aab5] #greyple
    cor = random.choice(listaCores)
    return cor



