import discord
import Util.getAnime   as getAnime
import Util.gerarCores as cor

def animeEmbed(animeId):
    title, url, status, episodios, image, bannerImage, tipo, generos, score, duracao, descricao = getAnime.animeInfo(animeId)

    if bannerImage == None:
        bannerImage = image
        
    if len(descricao) > 800:
        descricao = formatarDescricao(descricao)

    animeEmbed = discord.Embed(
         title = title,
         url   = url,
         color = cor.gerarCorRandom()
        )
    duracao, tipo, status, score, generos = formataDuracaoTipoStatus(duracao, tipo, status, score, generos)    
    animeEmbed.set_image(url     = bannerImage)
    animeEmbed.set_thumbnail(url = image)
    animeEmbed.add_field(name = '__Tipo__',      value = tipo,        inline = False)
    animeEmbed.add_field(name = '__Episodios__', value = episodios,   inline = True)
    animeEmbed.add_field(name = '__Status__',    value = status,      inline = True)
    animeEmbed.add_field(name = '__Gêneros:__',  value = generos,     inline = False)
    animeEmbed.add_field(name = '__Score__',     value = score,       inline = True)
    animeEmbed.add_field(name = '__Duração:__',  value = duracao,     inline = True)
    animeEmbed.add_field(name = '__Sinopse:__',  value = descricao,   inline = False)

    return animeEmbed

def formataDuracaoTipoStatus(duracao, tipo, status, score, generos):
    if  duracao != None:
        if  tipo == 'MOVIE':
            duracao = f"{duracao//60} hr(s) {duracao%60} min(s)"
        else:
            duracao = f"{duracao} min por ep"
    else:
            duracao = "*Não revelada*"

    if  status == 'NOT_YET_RELEASED':
            status  = "*Ainda não foi ao ar*"

    score   ='.'.join(str(score))
    generos =', '.join(map(str, generos))
    return duracao, tipo, status, score, generos

def formatarDescricao(descricao):
    descricao  = descricao[0:800]
    descricao += "..."
    return descricao