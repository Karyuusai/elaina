from discord import Embed
from Util.embedImages import getImage
from Util.gerarCores import gerarCorRandom

def simpleEmbed(title, description, author, thumbnail):
    embed = Embed(title=title, description=description, color=gerarCorRandom())
    embed.set_author(name = author.name, icon_url = author.avatar_url)
    embed.set_thumbnail(url = getImage(thumbnail))

    return embed