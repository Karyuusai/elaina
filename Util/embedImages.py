images = {
    "CommandNotFound":"https://avatarfiles.alphacoders.com/108/108676.gif",
    "MissingPermissions":"https://avatarfiles.alphacoders.com/107/107146.gif",
    "SaveBackup":"https://avatarfiles.alphacoders.com/108/108672.gif",
    "SomethingWrong":"https://avatarfiles.alphacoders.com/108/108190.gif",
    "Backup":"https://avatarfiles.alphacoders.com/110/110058.gif",
    "NSFW":"https://avatarfiles.alphacoders.com/110/110003.gif"
}

def getImage(key):
    return images[key]

    