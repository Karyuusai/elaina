from pymongo import MongoClient
from dotenv import load_dotenv
import os
load_dotenv()

class DataBase():
    def __init__(self):
        self.connectionString = os.getenv("CONNECTION_STRING")
        self.client   = MongoClient(self.connectionString)
        self.db       = self.client.dataBase
        self.prefix   = self.db['prefix']
        self.saves    = self.db['saves']

    def get_prefix(self, m):
        collection = self.db['prefix']
        itens = collection.find_one({"_id":int(m.guild.id)})
        return str(itens["Prefix"])

    def save(self, ctx, save):
        collection = self.saves
        itens = collection.find()
        for item in itens:
            if item["_id"] == ctx.author.guild.id:
                collection.find_one_and_delete({"_id":int(ctx.author.guild.id)})
        collection.insert_one(save)
    
    def find(self, ctx):
        collection = self.saves
        itens = collection.find_one({"_id":int(ctx.author.guild.id)})
        return dict(itens)



