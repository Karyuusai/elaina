import os
from discord.ext import commands
from discord import Intents
from dataBase.dataBase import DataBase
from dotenv import load_dotenv

load_dotenv()

db = DataBase()

intentss = Intents.all()

def prefix(bot, message):
    p = db.get_prefix(message)
    return commands.when_mentioned_or(p)(bot, message)

bot = commands.Bot(command_prefix=prefix, intents = intentss)

@bot.event
async def on_ready():
    print(f"{bot.user} has online")

    for cogName in os.listdir("cogs"):
        if cogName.endswith('.py'):
            bot.load_extension(f'cogs.{cogName[:-3]}')

bot.run(os.getenv("BOT_TOKEN"))




